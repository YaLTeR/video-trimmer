msgid ""
msgstr ""
"Project-Id-Version: Video Trimmer\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/YaLTeR/video-trimmer/-/"
"issues\n"
"POT-Creation-Date: 2024-09-20 19:08+0000\n"
"PO-Revision-Date: \n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: \n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.4.2\n"

#. Can't extract filenames from a VariantDict yet.
#. Translators: --output commandline option description.
#: src/application.rs:37
msgid "Output file path"
msgstr "Emplaçament del fichièr de sortida"

#. Translators: --output commandline option arg description.
#: src/application.rs:39
msgid "PATH"
msgstr "EMPLAÇAMENT"

#: src/main.rs:40 src/window.blp:27
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:3
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:5
msgid "Video Trimmer"
msgstr "Copador de vidèo"

#: src/shortcuts.blp:9
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: src/shortcuts.blp:12
msgctxt "shortcut window"
msgid "New window"
msgstr "Fenèstra novèla"

#: src/shortcuts.blp:17
msgctxt "shortcut window"
msgid "Play / Pause"
msgstr "Lectura / pausa"

#: src/shortcuts.blp:22
msgctxt "shortcut window"
msgid "Trim"
msgstr "Copar"

#: src/shortcuts.blp:27
msgctxt "shortcut window"
msgid "Set Start at current postion"
msgstr "Definir la debuta a la posicion actuala"

#: src/shortcuts.blp:32
msgctxt "shortcut window"
msgid "Set End at current position"
msgstr "Definir la fin a la posicion actuala"

#: src/shortcuts.blp:37
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Acorchis clavièr"

#: src/shortcuts.blp:42
msgctxt "shortcut window"
msgid "Close window"
msgstr "Tampar la fenèstra"

#: src/shortcuts.blp:47
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quitar"

#. Translators: Title that is shown if we couldn't load the video track for the opened file (but possibly loaded audio).
#: src/video_preview.blp:17
msgid "Could Not Load Video Track"
msgstr "Cargament impossible de la pista vidèo"

#. Translators: Description that is shown if we couldn't load the video track for the opened file (but possibly loaded audio).
#: src/video_preview.blp:20
msgid ""
"Maybe the file only has an audio track, or maybe the necessary video codec "
"is missing."
msgstr ""
"Benlèu que lo fichièr a pas qu'una pista àudio, o que lo codec necessari es "
"absent."

#. Translators: file chooser file filter name.
#: src/window.rs:214
msgid "Video files"
msgstr "Fichièrs vidèo"

#. Translators: file chooser dialog title.
#: src/window.rs:221
msgid "Open video"
msgstr "Dobrir la vidèo"

#. Translators: error dialog title.
#: src/window.rs:232 src/window.rs:412 src/window.rs:661
msgid "Error"
msgstr "Error"

#. Translators: error dialog text.
#: src/window.rs:235 src/window.rs:415 src/window.rs:664
msgid ""
"Video Trimmer can only operate on local files. Please choose another file."
msgstr ""
"Video Trimmer pòt sonque foncionar amb de fichièrs locals. Mercés de causir "
"un autre fichièr."

#. Translators: error dialog button.
#: src/window.rs:240 src/window.rs:420 src/window.rs:603 src/window.rs:625
#: src/window.rs:669
msgid "_OK"
msgstr "_D'acòrdi"

#. Translators: this is appended to the output video file name.
#. So for example "my video.mp4" will become "my video (trimmed).mp4".
#: src/window.rs:354
msgid " (trimmed)"
msgstr " (copada)"

#. Translators: checkbox in output file selection dialog that strips audio from the
#. video file.
#: src/window.rs:381
msgid "Remove audio"
msgstr "Tirar l’àudio"

#. Translators: checkbox in output file selection dialog.
#: src/window.rs:386
msgid "Accurate trimming, but slower and may lose quality"
msgstr "Copa precisa, mas mai lenta e pèrda de qualitat possibla"

#. Translators: message dialog text.
#: src/window.rs:513
msgid "Trimming…"
msgstr "Copadura…"

#. Translators: trimming dialog button.
#: src/window.rs:516
msgid "_Cancel"
msgstr "_Anullar"

#. Translators: text on the toast after trimming was done.
#. The placeholder is the video filename.
#: src/window.rs:536
msgid "{} has been saved"
msgstr "{} es estat enregistrat"

#. Translators: text on the button of the toast after
#. trimming was done to show the output file in the file
#. manager.
#: src/window.rs:543
msgid "Show in Files"
msgstr "Mostrar dins Fichièrs"

#. Translators: error dialog heading.
#: src/window.rs:575
msgid "Error trimming video"
msgstr "Error en copant la vidèo"

#. Translators: error dialog text before the FFmpeg
#. error output.
#: src/window.rs:579
msgid "Please attach the following information when reporting an issue."
msgstr ""
"Mercés d'ajustar las informacions seguentas al moment de senhalar un "
"problèma."

#: src/window.rs:590
msgid "Could not communicate with the ffmpeg subprocess"
msgstr "Comunicacion impossibla amb lo jos processús ffmpeg"

#. Translators: error dialog text.
#: src/window.rs:621
msgid "Could not create the ffmpeg subprocess"
msgstr "Creacion impossibla del jos processús ffmpeg"

#. Translators: shown in the About dialog, put your name here.
#: src/window.rs:831
msgid "translator-credits"
msgstr "Quentin PAGÈS"

#. Translators: link title in the About dialog.
#: src/window.rs:834
msgid "Contribute Translations"
msgstr "Participar a las traduccions"

#: src/window.rs:842
msgid ""
"This release improves the behavior of shortcuts and does a minor visual "
"refresh for GNOME 47."
msgstr "Aquesta version conten de refrescaments visuals menors per GNOME 47."

#: src/window.rs:843
msgid "Added I and O shortcuts to set start and end trimming point."
msgstr ""
"Apondon dels acorchis I e O per definir la debuta e fin de l'interval a "
"copar."

#: src/window.rs:844
msgid ""
"Made all single-letter shortcuts work even when the time input fields are "
"focused."
msgstr ""
"Fach foncionar totes los acorchis d'una sola letra quitament quand los "
"camps d'entrada del temps son actius."

#: src/window.rs:845
msgid "Changed the play/pause Ctrl+Space shortcut to just Space."
msgstr ""
"Modificacion de l’acorchi clavièr Ctrl + Espaci per bascular en lectura/"
"pausa per simplament Espaci."

#: src/window.rs:846
msgid "The video now starts paused after opening."
msgstr "La vidèo comença ara en pausa aprèp dobertura."

#: src/window.rs:847
msgid "Updated to the GNOME 47 platform."
msgstr "Actualizat per la platafòrma GNOME 47."

#: src/window.rs:848
msgid "Updated translations."
msgstr "Traduccions actualizadas."

#. Translators: Primary menu entry that opens a new window.
#: src/window.blp:8
msgid "_New Window"
msgstr "Fenèstra _novèla"

#. Translators: Primary menu entry that opens the Keyboard Shortcuts dialog.
#: src/window.blp:14
msgid "_Keyboard Shortcuts"
msgstr "_Acorchis clavièr"

#. Translators: Primary menu entry that opens the About dialog.
#: src/window.blp:20
msgid "_About Video Trimmer"
msgstr "_A prepaus del Copador vidèo"

#. Translators: Main menu button tooltip.
#: src/window.blp:50 src/window.blp:106
msgid "Main Menu"
msgstr "Menú principal"

#. Translators: Title text on the window when no files are open.
#: src/window.blp:59
msgid "Trim Videos"
msgstr "Copar de vidèos"

#. Translators: Description text on the window when no files are open.
#: src/window.blp:62
msgid "Drag and drop a video here"
msgstr "Lissar depausar una vidèo aicí"

#. Translators: Open button label when no files are open.
#: src/window.blp:66
msgid "Open Video…"
msgstr "Dobrir una vidèo…"

#. Translators: Open button tooltip.
#: src/window.blp:68
msgid "Select a Video"
msgstr "Seleccionar una vidèo"

#. Translators: Trim button.
#: src/window.blp:92
msgid "Trim"
msgstr "Copar"

#. Translators: Trim button tooltip.
#: src/window.blp:95
msgid "Trim Video"
msgstr "Copar vidèo"

#. Translators: Title that is shown upon video preview loading error.
#: src/window.blp:134
msgid "Video Preview Failed to Load"
msgstr "Fracàs del cargament de la previsualizacion de la vidèo"

#. Translators: Description that is shown upon video preview loading error.
#: src/window.blp:137
msgid ""
"Please enter the start and end timestamps manually.\n"
"\n"
"If you're running Video Trimmer under Flatpak, note that opening files by "
"drag-and-drop may not work."
msgstr ""
"Mercés de picar la debuta e la fin a la man.\n"
"\n"
"S'utilizatz Video Trimmer jos flatpak, notatz que la dobertura de fichièr "
"via lissar depausar pòt foncionar pas."

#. Translators: Label for the entry for the start timestamp.
#: src/window.blp:157
msgid "Start"
msgstr "Debuta"

#. Translators: Label for the entry for the end timestamp.
#: src/window.blp:174
msgid "End"
msgstr "Fin"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:4
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:6
msgid "Trim videos quickly"
msgstr "Copar de vidèos rapidament"

#. Translators: search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:6
msgid "Cut;Movie;Film;Clip;"
msgstr "Copar;Films;Clip;Vidèo;"

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:13
msgid "Ivan Molodetskikh"
msgstr "Ivan Molodetskikh"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:23
msgid ""
"Video Trimmer cuts out a fragment of a video given the start and end "
"timestamps. The video is never re-encoded, so the process is very fast and "
"does not reduce the video quality."
msgstr ""
"Lo Copador de vidèos retalha un fragement d’una vidèo segon los ponches de "
"debuta e de fin donats. La vidèo es pas jamai reencodada, fa que lo "
"processús es rapid e redusís pas la qualitat de la vidèo."

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:30
msgid "Main Window"
msgstr "Fenèstra principala"

#~ msgid "Tweaked the visual style for the GNOME 45 release."
#~ msgstr "Estil visual adaptat per la version 45 de GNOME."

#~ msgid "Fixed app crashing when ffprobe is missing."
#~ msgstr "Correccion del plantatge de l’aplicacion quand ffprobe es absent."

#~ msgid "Press the Open button to open a video file."
#~ msgstr "Quichatz lo boton Dobrir per dobrir un fichièr vidèo."

#~ msgid "Open"
#~ msgstr "Dobrir"

#~ msgid "Added a Dutch translation."
#~ msgstr "Traduccion en neerlandés aponduda"

#~ msgid "First release."
#~ msgstr "Primièra version."

#~ msgid "Updated Spanish translation."
#~ msgstr "Actualizacion de la traduccion en espanhòl."

#~ msgid "Added Turkish translation."
#~ msgstr "Traduccion en turc aponduda."
